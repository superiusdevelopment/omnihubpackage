<?php

namespace Superius\OmniHub\Helpers;

/**
 * @method static HubApiUserRequest billingHub()
 * @method static HubApiUserRequest billingRsHub()
 * @method static HubApiUserRequest cclHub()
 * @method static HubApiUserRequest commissionsHub()
 * @method static HubApiUserRequest customersHub()
 * @method static HubApiUserRequest dunningHub()
 * @method static HubApiUserRequest faqHub()
 * @method static HubApiUserRequest fmHub()
 * @method static HubApiUserRequest invoicesHub()
 * @method static HubApiUserRequest minimaxHub()
 * @method static HubApiUserRequest hub()
 * @method static HubApiUserRequest neosalonHub()
 * @method static HubApiUserRequest nextstepHub()
 * @method static HubApiUserRequest pantheonHub()
 * @method static HubApiUserRequest partnersHub()
 * @method static HubApiUserRequest phoneHub()
 * @method static HubApiUserRequest quotes2Hub()
 * @method static HubApiUserRequest statementsHub()
 * @method static HubApiUserRequest subscriptionsHub()
 * @method static HubApiUserRequest ticketsHub()
 * @method static HubApiUserRequest utilitiesHub()
 * @method static HubApiUserRequest warehousesHub()
 * @method static HubApiRequest setupHub()
 * @method static HubApiRequest reportingHub()
 */
class HubApiUserRequestBuilder extends HubUrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return \Superius\OmniHub\Helpers\HubApiUserRequest
     */
    public static function __callStatic(string $name, array $arguments): HubApiUserRequest
    {

        return self::apiRequest(self::formatUrlKey($name));
    }

    private static function apiRequest(string $hubUrlKey): HubApiUserRequest
    {
        $url = self::getUrlByKey($hubUrlKey);

        return new HubApiUserRequest($url);
    }
}
