<?php

namespace Superius\OmniHub\A2ARequest;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

abstract class A2ARequest
{
    private $host;
    private $uri;
    private $token;

    public function __construct(string $host = null)
    {
        $this->host = $host;
    }

    /**
     * @throws \JsonException
     */
    public function getJson(string $uri, bool $returnError = false): array
    {
        $response = $this->get($uri);

        if (!$returnError && $response->clientError()) {
            return [];
        }
        return $this->jsonDecode($response->body());
    }

    public function get(string $uri): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->get($this->getUrl());
    }

    private function getRequest(): PendingRequest
    {
        if (!isset($this->token) || !$this->token) {
            $this->token = $this->getToken();
        }
        return Http::withToken($this->token)->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ]);
    }

    abstract function getToken(): string;

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    private function getUrl(): string
    {
        $url = '';
        if ($this->host) {
            $url .= $this->host;
        } else {
            $url .= config('app.url');
        }

        if ($this->uri) {
            $url .= $this->uri;
        } else {
            throw new \RuntimeException('Trying to make request with empty uri');
        }
        return $url;
    }

    private function jsonDecode(string $input): array
    {
        return json_decode($input, true, 512, JSON_THROW_ON_ERROR);
    }

    public function postJson(string $uri, array $data): array
    {
        $response = $this->post($uri, $data);
        return $this->jsonDecode($response->body());
    }

    public function post(string $uri, array $data): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->post($this->getUrl(), $data);
    }

    public function put(string $uri, array $data): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->put($this->getUrl(), $data);
    }

    public function putJson(string $uri, array $data): array
    {
        $response = $this->put($uri, $data);
        return $this->jsonDecode($response->body());
    }
}
