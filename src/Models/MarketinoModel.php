<?php

namespace Superius\OmniHub\Models;

use Superius\OmniHub\Models\Traits\HasUuid;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Superius\OmniHub\Scopes\MarketScope;
use Superius\OmniHub\Helpers\MarketContext;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

abstract class MarketinoModel extends Model
{
    use HasUuid;
    use SoftDeletes;
    use HasFactory;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

            //allowing seeders to set mid directly
            if ($model->mid && App::runningInConsole()) {
                return;
            }

            $model->mid = MarketContext::getInstance()->getMarketId();
        });
    }

    protected static function booted()
    {
        static::addGlobalScope(new MarketScope);
    }
}
