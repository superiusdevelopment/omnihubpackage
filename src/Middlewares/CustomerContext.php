<?php


namespace Superius\OmniHub\Middlewares;

use App;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Superius\OmniHub\Enums\ContextTypeEnum;

class CustomerContext
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->cc) {
            URL::defaults(['cc' => $request->cc]);

            if (Str::startsWith($request->cc, 'cc/')) {
                $request->content_entity = ContextTypeEnum::CUSTOMER->value;
            }

            if (Str::startsWith($request->cc, 'pc/')) {
                $request->content_entity = ContextTypeEnum::PARTNER->value;
            }

            $request->cc = Str::replace(['cc/', 'pc/'], '', $request->cc);
        }

        return $next($request);
    }
}
