<?php

namespace Superius\OmniHub\Traits;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

trait Uuidable
{
    public function uuid(): UuidInterface
    {
        return Uuid::uuid5(Uuid::NAMESPACE_OID, $this->value);
    }
}
