<?php

namespace Superius\OmniHub\Enums;

enum UserGroupEnum: string
{
    case ADMIN = 'admin';
    case USER = 'user';
    case SUPPORT = 'support';

    public static function asArray(): array
    {
        return array_reduce(self::cases(), function ($options, $case) {
            $options[$case->name] = $case->value;

            return $options;
        }, []);
    }
}
