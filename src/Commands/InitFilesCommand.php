<?php

namespace Superius\OmniHub\Commands;

use Illuminate\Console\Command;

/**
 * Class InitFilesCommand
 * @package Superius\OmniHub\Commands
 * copy shared files in new hub
 */
class InitFilesCommand extends Command
{
    protected $signature = 'omnihub:initfiles';
    protected $description = 'Prepare init files';

    public function __construct()
    {
        parent::__construct();
    }

    final public function handle(): void
    {
        $initFilesPackagePath = 'vendor/superius/omnihub/init_files';
        if(!file_exists($initFilesPackagePath) || !is_dir($initFilesPackagePath)){
            throw new \RuntimeException(sprintf('Directory "%s" is missing or invalid', $initFilesPackagePath));
        }
        $initFilesPackageDir = scandir($initFilesPackagePath);

        foreach ($initFilesPackageDir as $file){
            if($file === '.' || $file === '..') continue;
            copy($initFilesPackagePath.'/'.$file,getcwd().'/'.basename($file));
        }
    }
}
