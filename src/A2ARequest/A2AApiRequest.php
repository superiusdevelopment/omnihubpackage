<?php

namespace Superius\OmniHub\A2ARequest;

class A2AApiRequest extends A2ARequest
{
    private ?int $mid = null;
    private ?string $tid = null;
    private bool $isDemo = false;

    /**
     * @throws \JsonException
     */
    public function getToken(): string
    {
        return (new A2AApiToken($this->mid, $this->tid, $this->isDemo))->toString();
    }

    public static function validateToken(string $token): bool
    {
        return (new A2AApiToken())->fromToken($token)->isValid();
    }

    public function addTenantContext(int $mid, string $tid, bool $isDemo = false): void
    {
        $this->mid = $mid;
        $this->tid = $tid;
        $this->isDemo = $isDemo;
    }
}
