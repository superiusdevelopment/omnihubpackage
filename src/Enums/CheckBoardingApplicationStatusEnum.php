<?php

namespace Superius\OmniHub\Enums;

enum CheckBoardingApplicationStatusEnum: string
{
    case DRAFT = 'DRAFT';
    case UNDER_REVIEW = 'UNDER_REVIEW';
    case APPROVED = 'APPROVED';
    case UNKNOWN = 'UNKNOWN';
}
