<?php

namespace Superius\OmniHub\Enums;

enum WarehouseItemTypeEnum: string
{
    case SIM_CARD = 'sim_card';
    case CASH_REGISTER = 'cash_register';
}
