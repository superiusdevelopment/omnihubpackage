<?php

namespace Superius\OmniHub\Models;

use Superius\OmniHub\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Relations\Pivot;

abstract class UuidPivot extends Pivot
{
    use HasUuid;
}
