<?php

namespace Superius\OmniHub\View\Components;

use Cache;
use Auth;
use Illuminate\View\Component;
use Superius\OmniHub\Helpers\MarketContext;
use Superius\OmniHub\Helpers\HubApiUserRequestBuilder;

class Header extends Component
{
    public function render()
    {
        $userId = data_get(Auth::user(), 'id');
        $mid = MarketContext::getInstance()->getMarketId();
        return Cache::remember('motherhub-header-'.$userId.'-'.$mid, config('omnihub.cache.lifetime'), function () {
            $headerMenu = HubApiUserRequestBuilder::hub()->getJson('/api/layouts/header');
            return data_get($headerMenu, 'html');
        });
    }
}
