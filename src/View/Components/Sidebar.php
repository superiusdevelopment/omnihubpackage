<?php

namespace Superius\OmniHub\View\Components;

use Illuminate\View\Component;
use Superius\OmniHub\Helpers\MarketContext;
use Superius\OmniHub\Helpers\HubApiUserRequestBuilder;
use Cache;
use Auth;

class Sidebar extends Component
{
    public function render()
    {
        $userId = data_get(Auth::user(), 'id');
        $mid = MarketContext::getInstance()->getMarketId();

        return Cache::remember(
            'motherhub-sidebar-' . $userId . '-' . $mid,
            config('omnihub.cache.lifetime'),
            function () {
                $sidebarMenu = HubApiUserRequestBuilder::hub()->getJson('/api/layouts/sidebar');
                return data_get($sidebarMenu, 'html');
            }
        );
    }
}
