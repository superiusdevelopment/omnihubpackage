<?php

namespace Superius\OmniHub\Models\Traits;

use Superius\OmniHub\Helpers\HubApiHelper;

trait HasCustomer
{
    public function getCustomerDataAttribute()
    {
        $customer = [];
        if ($this->customer_id) {
            $customer = HubApiHelper::getCustomer($this->customer_id);
        }
        return $customer;
    }
}
