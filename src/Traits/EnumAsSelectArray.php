<?php

namespace Superius\OmniHub\Traits;

trait EnumAsSelectArray
{
    /**
     * Get an associative array of [case value => case value].
     * @param array<int,mixed> $cases
     * @return array<mixed>
     */
    public static function asSelectArray(array $cases): array
    {
        return array_reduce($cases, function ($options, $case) {
            $options[$case->value] = $case->value;

            return $options;
        }, []);
    }
}
