<?php

namespace Superius\OmniHub\View\Components;

use Cache;
use Illuminate\View\Component;
use Superius\OmniHub\Enums\ContextTypeEnum;
use Superius\OmniHub\Helpers\HubApiUserRequestBuilder;

class CustomerHeader extends Component
{
    public function render()
    {
        if (request()->cc) {
            if (request()->content_entity === ContextTypeEnum::PARTNER->value) {
                return Cache::remember('partner-header-' . request()->cc, config('omnihub.cache.lifetime'), function () {
                    $partnerHeader = HubApiUserRequestBuilder::partnersHub()->getJson('/api/layouts/partner-header/' . request()->cc);
                    return data_get($partnerHeader, 'html');
                });
            }

            return Cache::remember('customer-header-' . request()->cc, config('omnihub.cache.lifetime'), function () {
                $customerHeader = HubApiUserRequestBuilder::customersHub()->getJson('/api/layouts/customerheader/' . request()->cc);
                return data_get($customerHeader, 'html');
            });
        }
        return '';
    }
}
