<?php


namespace Superius\OmniHub\Helpers;

use Tests\TestCase;
use App\Models\User;
use Superius\OmniHub\Enums\MarketEnum;
use function _PHPStan_5c71ab23c\React\Promise\map;

trait TestAuth
{
    public $authUser;

    /*
     * usage in tests:
     *
     * use Superius\OmniHub\Helpers\TestAuth;
     * class SomeTest extends TestCase {
     *      use TestAuth;
     * ....
     *
     * a) $this->apiAuth()->json('POST', '/api/send',...
     *
     * b) $this->apiAuth();
     *    $this->json('POST', '/api/send',...
     */

    protected function apiAuth(array $userData = [], ?string $market = null): TestCase
    {
        return $this->makeAuth($userData, $market, 'api');
    }

    protected function auth(array $userData = [], ?string $market = null): TestCase
    {
        return $this->makeAuth($userData, $market);
    }

    private function makeAuth(array $userData = [], ?string $market = null, string $middleware = null): TestCase
    {
        $this->setMarketContext($market);

        if ($userData || !$this->authUser) { // Reuse auth user
            $this->authUser = User::factory()->make($userData);
        }

        return $this->actingAs($this->authUser, $middleware);
    }

    /*
    * usage in tests:
    *
    * use Superius\OmniHub\Helpers\TestAuth;
    * class SomeTest extends TestCase {
    *      use TestAuth;
    * ....
    *
    * $response = $this->apiAuth()->getJson('/api/send',...
    */

    public function withTimeToken(?string $market = null): TestCase
    {
        $this->setMarketContext($market);

        return $this->withHeaders([
            'Authorization' => "Bearer " . HubApiRequest::getToken(),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ]);
    }

    private function setMarketContext(?string $market = null): void
    {
        if ($market) {
            MarketContext::set($market);
        } else if (MarketContext::isNotSet()) {
            MarketContext::set(MarketEnum::HR->value);
        }
    }
}
