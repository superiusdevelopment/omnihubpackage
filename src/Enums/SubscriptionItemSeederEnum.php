<?php

namespace Superius\OmniHub\Enums;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Služi da bi se šerali id-jevi subscription itema
 * Ovaj isti enum se nalazi na invoices i subscription hubu
 */

enum SubscriptionItemSeederEnum: string
{
    case MONTHLY_BILLING_CASH_REGISTER = 'Subscription:SubscriptionItem:Usluga mjesečnog održavanja fiskalne blagajne';
    case MONTHLY_BILLING_ADDON_PLUS = 'Subscription:SubscriptionItem:Usluga mjesečnog održavanja dodataka za salone';
    case MONTHLY_BILLING_PLUS_SMS = 'Subscription:SubscriptionItem:Usluga slanja SMS podsjetnika';
    case MONTHLY_BILLING_MOBILE_INTERNET = 'Subscription:SubscriptionItem:Internet opcija';
    case MONTHLY_BILLING_DEVICE_RENTAL_V2S = 'Subscription:SubscriptionItem:Najam uređaja V2S';
    case MONTHLY_BILLING_CARD_PAYMENT_SERVICE = 'Subscription:SubscriptionItem:Usluga kartičnog plaćanja';
    case MONTHLY_BILLING_SI_MARKETINO = 'Subscription:SubscriptionItem:Si:Storitev mesečnega vzdrževanja';

    public function uuid(): UuidInterface
    {
        return Uuid::uuid5(Uuid::NAMESPACE_OID, $this->value);
    }
}
