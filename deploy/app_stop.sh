#!/bin/bash

cd ##PATH##

if [ -d "vendor" ]; then
  php artisan down
  php artisan config:clear
fi
