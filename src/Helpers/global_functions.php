<?php

use Superius\OmniHub\Helpers\HubUrlBuilder;

if (!function_exists('omnihub_asset')) {
    function omnihub_asset($path)
    {
        return app('url')->assetFrom(HubUrlBuilder::hub(), $path);
    }
}

if (!function_exists('isHrMarket')) {
    function isHrMarket(): bool
    {
        $market = Superius\OmniHub\Helpers\MarketContext::getInstance()->getMarketId();
        return $market === Superius\OmniHub\Enums\MarketEnum::HR->value;
    }

    function isItMarket(): bool
    {
        $market = Superius\OmniHub\Helpers\MarketContext::getInstance()->getMarketId();
        return $market === Superius\OmniHub\Enums\MarketEnum::IT->value;
    }

    function isRsMarket(): bool
    {
        $market = Superius\OmniHub\Helpers\MarketContext::getInstance()->getMarketId();
        return $market === Superius\OmniHub\Enums\MarketEnum::RS->value;
    }

    function isSiMarket(): bool
    {
        $market = Superius\OmniHub\Helpers\MarketContext::getInstance()->getMarketId();
        return $market === Superius\OmniHub\Enums\MarketEnum::SI->value;
    }

    function isUsMarket(): bool
    {
        $market = Superius\OmniHub\Helpers\MarketContext::getInstance()->getMarketId();
        return $market === Superius\OmniHub\Enums\MarketEnum::US->value;
    }
}
