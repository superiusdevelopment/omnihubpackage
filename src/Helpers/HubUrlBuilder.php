<?php

namespace Superius\OmniHub\Helpers;

/**
 * @method static string billingHub()
 * @method static string billingRsHub()
 * @method static string cclHub()
 * @method static string commissionsHub()
 * @method static string customersHub()
 * @method static string dunningHub()
 * @method static string faqHub()
 * @method static string fmHub()
 * @method static string invoicesHub()
 * @method static string minimaxHub()
 * @method static string hub()
 * @method static string neosalonHub()
 * @method static string nextstepHub()
 * @method static string pantheonHub()
 * @method static string partnersHub()
 * @method static string phoneHub()
 * @method static string quotes2Hub()
 * @method static string statementsHub()
 * @method static string subscriptionsHub()
 * @method static string ticketsHub()
 * @method static string utilitiesHub()
 * @method static string warehousesHub()
 * @method static string setupHub()
 * @method static string reportingHub()
 */
class HubUrlBuilder extends HubUrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return string
     */
    public static function __callStatic(string $name, array $arguments): string
    {
        return self::getUrlByKey(self::formatUrlKey($name));
    }
}
