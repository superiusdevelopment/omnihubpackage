<?php

namespace Superius\OmniHub\Helpers;

class Patterns
{
    public const UUID = '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}';
    public const CC = '[p|c]c/' . self::UUID;
    public const MARKET = 'hr|si|rs|it';
}
