<?php

namespace Superius\OmniHub\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateWhitelistRequest extends FormRequest
{
    /**
     * @return array<string,array<mixed>>
     */
    public function rules(): array
    {
        return [
            'ip_address' => ['sometimes', 'nullable', 'string', 'ip'],
        ];
    }
}
