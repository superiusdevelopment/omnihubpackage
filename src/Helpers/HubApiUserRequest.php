<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;

/**
 * Class HubApiUserRequest
 *
 * @package App\Helpers
 * helper for making api request with auth user to hubs
 */
class HubApiUserRequest extends HubApi
{
    public static function getToken(): string
    {
        $sessionId = session()->getId();
        if (!$sessionId || Auth::guest()) {
            throw new \RuntimeException('Request expect auth user with session');
        }

        return Crypt::encryptString(session()->getId());
    }

    public static function getRequest(): \Illuminate\Http\Client\PendingRequest
    {
        return Http::withToken(static::getToken())->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ]);
    }
}
