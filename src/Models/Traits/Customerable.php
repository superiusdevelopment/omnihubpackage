<?php

namespace Superius\OmniHub\Models\Traits;

use Superius\OmniHub\Enums\ContextTypeEnum;
use Superius\OmniHub\Helpers\HubUrlBuilder;

class Customerable
{
    public readonly string $name;
    public readonly string $url;

    public function __construct(
        public readonly string $id,
        public readonly int    $counter_id,
        public readonly int    $mid,
        public readonly string $first_name,
        public readonly string $last_name,
        public readonly string $company_name,
        public readonly string $vat_number,
        public readonly string $address,
        public readonly string $address_additional,
        public readonly string $zip,
        public readonly string $city,
        public readonly string $state,
        public readonly string $country_code,
        public readonly string $default_email,
        public readonly string $default_phone,
        public readonly string $marketino_number,
        public readonly ?string $type,
        public readonly string $created_at,
    )
    {
        $this->name = $company_name ?: (implode(' ', [$first_name, $last_name]));

        $this->url = match (true) {
            $id && $this->isCustomer() => HubUrlBuilder::customersHub() . '/' . $id,
            $id && $this->isPartner() => HubUrlBuilder::partnersHub() . '/' . $id,
            default => '#',
        };
    }

    public function isPartner(): bool
    {
        return $this->type == ContextTypeEnum::PARTNER->value;
    }

    public function isCustomer(): bool
    {
        return $this->type == ContextTypeEnum::CUSTOMER->value;
    }

    public static function make(array $customerableData, ?string $type = null): Customerable
    {
        return new Customerable(
            id: data_get($customerableData, 'id', '') ?: '',
            counter_id: data_get($customerableData, 'counter_id', 0) ?: 0,
            mid: data_get($customerableData, 'mid', 0) ?: 0,
            first_name: data_get($customerableData, 'first_name', '') ?: '',
            last_name: data_get($customerableData, 'last_name', '') ?: '',
            company_name: data_get($customerableData, 'company_name', '') ?: '',
            vat_number: data_get($customerableData, 'vat_number', '') ?: '',
            address: data_get($customerableData, 'address', '') ?: '',
            address_additional: data_get($customerableData, 'address_additional', '') ?: '',
            zip: data_get($customerableData, 'zip', '') ?: '',
            city: data_get($customerableData, 'city', '') ?: '',
            state: data_get($customerableData, 'state', '') ?: '',
            country_code: data_get($customerableData, 'country_code', '') ?: '',
            default_email: data_get($customerableData, 'default_email', '') ?: '',
            default_phone: data_get($customerableData, 'default_phone', '') ?: '',
            marketino_number: data_get($customerableData, 'marketino_number', '') ?: '',
            type: $type,
            created_at: data_get($customerableData, 'created_at', '') ?: '',
        );
    }
}
