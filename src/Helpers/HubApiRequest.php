<?php


namespace Superius\OmniHub\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;

class HubApiRequest extends HubApi
{
    public static function getToken(): string
    {
        $payload = [
            'market' => MarketContext::getInstance()->getMarketId(),
            'ts' => Carbon::now('UTC')->timestamp
        ];
        return Crypt::encrypt($payload);
    }

    public static function getRequest(): \Illuminate\Http\Client\PendingRequest
    {
        return Http::withToken(static::getToken())->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ]);
    }
}
