<?php

namespace Superius\OmniHub\Services\MailchimpService;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Newsletter;

class MailchimpService
{
    private string $listName;

    public function __construct(private string $email, private ?string $partnerType = null)
    {
        $listNameService = new GetListName();
        $this->listName = $listNameService->getListName($partnerType);
    }

    public function subscribe(): JsonResponse
    {
        $response = Newsletter::subscribeOrUpdate($this->email, [], $this->listName);

        if ($response === false) {
            Log::error('Cannot add or update subscribe for customer with email address: ' . $this->email .
                '. Error: ' . Newsletter::getLastError());
            return response()->json('Subscribe for this email is not possible.', 500);
        }
        return response()->json([]);
    }

    public function unsubscribe(): JsonResponse
    {
        $response = Newsletter::unsubscribe($this->email, $this->listName);

        if (data_get($response, 'status') !== 'unsubscribed') {
            Log::error('Cannot unsubscribe email address: ' . $this->email .
                '. Error: ' . Newsletter::getLastError());
            return response()->json('Unsubscribe for this email is not possible.', 500);
        }

        return response()->json([]);
    }
}
