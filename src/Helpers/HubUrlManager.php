<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Support\Str;

class HubUrlManager
{
    protected static function getUrlByKey(string $hubUrlKey, bool $isAwsLocal = false): string
    {
        $env = app()->environment();

        if ($env === 'production' || $env === 'staging') {
            if ($hubUrlKey === 'hub') {
                return $isAwsLocal ? 'http://hub' . config('omnihub.aws_local.url_domain') .
                    PATH_SEPARATOR . config('omnihub.aws_local.port') :
                    'https://' . Str::replaceFirst('.', '', config('session.domain'));
            }

            $url = "https://$hubUrlKey" . config('session.domain');

            return $isAwsLocal ? self::transferUrlToAwsLocal($url, $env) : $url;
        }

        return "http://$hubUrlKey.omnihub.test";
    }

    private static function transferUrlToAwsLocal(string $url, string $env): string
    {
        if ($env === 'production') {
            $httpUrl = Str::replaceFirst('https', 'http', $url);
        } elseif ($env === 'staging') {
            $httpUrl = Str::replaceFirst('https://', 'http://test-', $url);
        }

        return Str::replaceLast(config('session.domain'), config('omnihub.aws_local.url_domain') .
            PATH_SEPARATOR . config('omnihub.aws_local.port'), $httpUrl);
    }

    public static function formatUrlKey($urlKey): string
    {
        $formattedUrlKey = Str::snake($urlKey, '-');

        if (app()->environment('production') || app()->environment('staging')) {
            $formattedUrlKey = Str::replaceLast('-hub', '', $formattedUrlKey);
        }

        return $formattedUrlKey;
    }
}
