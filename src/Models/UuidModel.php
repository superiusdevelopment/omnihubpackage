<?php

namespace Superius\OmniHub\Models;

use Superius\OmniHub\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

abstract class UuidModel extends Model
{
    use HasUuid;
}
