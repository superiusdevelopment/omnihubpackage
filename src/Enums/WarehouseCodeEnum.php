<?php

namespace Superius\OmniHub\Enums;

enum WarehouseCodeEnum: string
{
    case MAIN = 'M';
    case USED_SALE = 'US';
    case USED_RENT = 'UR';
    case SOLD = 'S';
    case IN_RENT = 'R';
    case IN_RETURN = 'RET';
    case EXPENDITURE = 'E';
}
