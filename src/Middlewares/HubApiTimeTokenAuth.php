<?php


namespace Superius\OmniHub\Middlewares;


use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Crypt;
use Superius\OmniHub\Helpers\MarketContext;

class HubApiTimeTokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /*
         * Definiranje i kriptiranje timestampa koji služi kao token za povezivanje različitih hubova.
         * Token, osim timestampa, sadrži i ID marketa (koji se inače šalje u session keyu)
         */
        $token = $request->bearerToken();

        if (!$token) {
            throw new AuthenticationException('Invalid api key (1)');
        }

        $payload = Crypt::decrypt($token);

        $ts = data_get($payload, 'ts');
        $marketId = data_get($payload, 'market');

        if (!$ts || !$marketId) {
            throw new AuthenticationException('Invalid api key (2)');
        }

        $date = Carbon::createFromTimestampUTC($ts);

        /*
         * Provjera da li je u requestu poslan ispravan format datuma te da li je razlika od slanja requesta pa
         * do zaprimanja manja od 5 sekundi.
         */
        if (!$date->isValid() || $date->diffInSeconds() > 5) {
            throw new AuthenticationException('Invalid api key (3)');
        }

        MarketContext::getInstance()->setMarket($marketId);

        return $next($request);
    }
}