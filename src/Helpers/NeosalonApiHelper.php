<?php

namespace Superius\OmniHub\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;

class NeosalonApiHelper
{
    /**
     * @param string $method
     * @param array<string,mixed> $data
     * @return Response
     */
    public static function post(string $method, array $data, string $market = 'hr'): Response
    {
        data_set($data, 'Signature', static::generateSignature());

        $url = config('omnihub.url.neosalon.' . $market) . '/domain/plugin/TenantApi/' . $method . '/execute';

        if (App::Environment() !== 'production') {
            Http::fake([$url => Http::response(['_status' => 'success'])]);
        }

        return Http::withHeaders([
            'Content-Type' => 'application/json',
        ])->post($url, $data);
    }

    private static function generateSignature(): string
    {
        return md5(config('omnihub.neosalon_api_key') . ':' . Carbon::now()->format('Y-m-d H'));
    }
}
