<?php

namespace Superius\OmniHub\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendMailService
{
    /**
     * @param array<string,mixed> $mailData
     */
    public function __construct(array $mailData)
    {
        $sendEmail = Http::withToken(config('omnihub.mail_api_token'))
            ->post(config('omnihub.url.mailapp') . '/api/send', $mailData);

        if ($sendEmail->failed()) {
            Log::error('Cannot send email. Status: ' . $sendEmail->status() . '. Body: ' . $sendEmail->body());
            abort($sendEmail->status(), 'Cannot send email');
        }
    }
}
