<?php

namespace Superius\OmniHub\Enums;

enum EventPolymorphicTypeEnum: string
{
    case CUSTOMER_SUBSCRIPTION = 'customer_subscription';
    case CUSTOMER_SUBSCRIPTION_LOG = 'customer_subscription_log';
    case CUSTOMER_TRANSACTION = 'customer_transaction';
    case DELIVERY = 'delivery';
    case INVOICE = 'invoice';
    case INVOICE_BALANCE = 'invoice_balance';
    case INVOICE_ITEM = 'invoice_item';
    case INVOICE_ITEM_ACTION = 'invoice_item_action';
    case INVOICE_TRANSACTION = 'invoice_transaction';
    case ITEM = 'item';
    case ITEM_ACTION = 'item_action';
    case PREPAID_SUBSCRIPTION = 'prepaid_subscription';
    case PREPAID_SUBSCRIPTION_LOG = 'prepaid_subscription_log';
    case QUOTE = 'quote';
    case STATEMENT = 'statement';
    case STATEMENT_ITEM = 'statement_item';
    case SUBSCRIPTION = 'subscription';
    case USER = 'user';
}
