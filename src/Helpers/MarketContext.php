<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Support\Str;
use Superius\OmniHub\Enums\MarketEnum;

class MarketContext
{
    private static $instance;

    private $marketId;

    public static function getInstance(): MarketContext
    {
        if (null === self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    final public function setMarket(int $marketId): void
    {

        if (!$marketId) {
            throw new \RuntimeException('Trying to set empty mid!');
        }

        $this->marketId = $marketId;
    }

    final public function getMarketId(bool $orDie = true): ?int
    {

        if (!$this->marketId && session()->isStarted() && session()->has('market')) {
            $this->marketId = session()->get('market');
        }

        if (!$this->marketId && $orDie) {
            throw new \RuntimeException('mid not set or trying to get empty mid!');
        }
        return $this->marketId;
    }

    public static function getMarketSlug(): string
    {
        return Str::lower(MarketEnum::from(self::getInstance()->getMarketId() ?:
            abort(500, 'Mid not found'))->name);
    }

    /**
     * Setting market context
     * @param int $marketId MarketEnum->value
     * @return void
     */
    public static function set(int $marketId): void
    {
        MarketContext::getInstance()->setMarket($marketId);
    }

    public static function isSet(): bool
    {
        return !MarketContext::isNotSet();
    }

    public static function isNotSet(): bool
    {
        return MarketContext::getInstance()->getMarketId() === null;
    }
}
