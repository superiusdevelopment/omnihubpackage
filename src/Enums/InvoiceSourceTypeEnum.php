<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum InvoiceSourceTypeEnum: string
{
    use EnumAsSelectArray;

    case INVOICE = 'invoice';
    case RECURRING_INVOICE_DATA = 'recurring_invoice_data';
    case QUOTE = 'quote';
    case USER = 'user';
    case MINIMAX_SYNC = 'minimax_sync';
}
