<?php

namespace Superius\OmniHub\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Horizon\HorizonApplicationServiceProvider;
use Superius\OmniHub\Enums\HorizonFileNameEnum;

class HorizonServiceProvider extends HorizonApplicationServiceProvider
{
    /**
     * Register the Horizon gate.
     *
     * This gate determines who can access Horizon in non-local environments.
     *
     * @return void
     */
    protected function gate(): void
    {
        Gate::define('viewHorizon', function ($user = null) {
            if (($user && $user->isAdmin()) ||
                data_get($_SERVER, 'REMOTE_ADDR') === config('omnihub.debug.remote_address')) {
                return true;
            }

            $whitelistFile = HorizonFileNameEnum::WHITE_LIST_FILE->value;
            if (Storage::exists($whitelistFile)) {
                return Str::contains(Storage::get($whitelistFile), Request::ip());
            }

            return false;
        });
    }
}
