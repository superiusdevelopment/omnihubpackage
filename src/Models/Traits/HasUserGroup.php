<?php

namespace Superius\OmniHub\Models\Traits;

use Superius\OmniHub\Enums\UserGroupEnum;

trait HasUserGroup
{
    public function isAdmin(): bool
    {
        return $this->group === UserGroupEnum::ADMIN->value;
    }
}
