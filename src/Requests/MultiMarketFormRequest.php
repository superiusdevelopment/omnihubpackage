<?php


namespace Superius\OmniHub\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Superius\OmniHub\Helpers\MarketContext;

class MultiMarketFormRequest extends FormRequest
{
    protected $requestClass;

    public function __construct(array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null)
    {

        $market = MarketContext::getInstance()->getMarketId();

        $requestClass = data_get($this->marketRequests, $market);
        $this->requestClass = new $requestClass;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }
}
