<?php

namespace Superius\OmniHub\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendSmsService
{
    public function __construct(
        string $smsText,
        string $receiverId,
        string $phoneNumber,
        int $mid = 1,
        string $receiverType = 'Customer',
        string $senderType = 'Job',
        ?string $senderId = null,
    ) {
        if (!isHrMarket()) {
            return;
        }

        $messages = [];
        $messages[] = [
            'phone_number' => $phoneNumber,
            'body' => $smsText,
            'receiver_id' => $receiverId,
            'receiver_type' => $receiverType,
            'sender_type' => $senderType,
            'sender_id' => $senderId,
        ];

        $sendSms = Http::withToken(config('omnihub.sms_api_token'))
            ->accept('application/json')
            ->post(config('omnihub.url.smsapp') . '/api/send', [
                'mid' => $mid,
                'messages' => $messages,
            ]);

        if ($sendSms->failed()) {
            Log::error('Cannot send email. Status: ' . $sendSms->status() . '. Body: ' . $sendSms->body());
            abort($sendSms->status(), 'Cannot send email');
        }
    }
}
