<?php


namespace Superius\OmniHub\Helpers;

use Illuminate\Support\Facades\Http;

class HubApiLocalRequest extends HubApi
{
    public static function getToken(): string
    {
        return '';
    }

    public static function getRequest(): \Illuminate\Http\Client\PendingRequest
    {
        return Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Market' => MarketContext::getInstance()->getMarketId(false)
        ]);
    }

}
