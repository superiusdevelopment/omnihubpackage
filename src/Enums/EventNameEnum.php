<?php

namespace Superius\OmniHub\Enums;

enum EventNameEnum: string
{
    case ADD_ITEM_TO_DELIVERY_LIST = 'Add item to the delivery list';
    case ADD_MONTHS_ON_SUBSCRIPTION = 'Add months on subscription';
    case CREATE_CUSTOMER_TRANSACTION = 'Create customer transaction';
    case CREATE_INVOICE = 'Create invoice';
    case CREATE_STATEMENT = 'Create statement';
    case INVOICE_CANCELLATION = 'Invoice cancellation';
    case PROCEED_CUSTOMER_TRANSACTION = 'Proceed customer transaction';
    case PROCEED_ITEM_ACTION = 'Proceed item action';
    case START_CUSTOMER_SUBSCRIPTION = 'Start customer subscription';
}
