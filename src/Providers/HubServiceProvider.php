<?php

namespace Superius\OmniHub\Providers;

use Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Superius\OmniHub\Commands\BuildCommand;
use Superius\OmniHub\Commands\CheckEnvCommand;
use Superius\OmniHub\Commands\SetDebugModeCommand;
use Superius\OmniHub\Logging\CustomLogger;
use Superius\OmniHub\Logging\CustomLogHandler;
use Superius\OmniHub\Middlewares\DevApiAuthToken;
use Superius\OmniHub\Middlewares\LocalApiAuth;
use Superius\OmniHub\View\Components\FilePond;
use Superius\OmniHub\Commands\InitFilesCommand;
use Superius\OmniHub\View\Components\Header;
use Superius\OmniHub\View\Components\Sidebar;
use Superius\OmniHub\Guards\SessionTokenGuard;
use Superius\OmniHub\Commands\EnvBuildCommand;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Superius\OmniHub\Helpers\HubExceptionHandler;
use Superius\OmniHub\View\Components\CustomerHeader;
use Superius\OmniHub\Middlewares\HubApiTimeTokenAuth;

class HubServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //global functions
        require_once __DIR__ . '/../Helpers/global_functions.php';

        //getting user for session without db
        \Auth::provider('session-user', function () {
            return resolve(SessionUserProvider::class);
        });

        //validating session with token
        \Auth::extend('session_token', function () {
            return new SessionTokenGuard(app('request'));
        });

        //register view components
        $this->loadViewComponentsAs('omnihub', [
            Header::class,
            Sidebar::class,
            CustomerHeader::class,
            FilePond::class,
        ]);

        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'omnihub');

        //set up sentry reporting
        $this->app->bind(
            ExceptionHandler::class,
            HubExceptionHandler::class
        );

        //scripts that prepares build
        if ($this->app->runningInConsole()) {
            $this->commands([
                BuildCommand::class,
                EnvBuildCommand::class,
                InitFilesCommand::class,
                CheckEnvCommand::class,
                SetDebugModeCommand::class,
            ]);
        }

        config([
            'logging.channels.custom' => [
                'driver' => 'custom',
                'handler' => CustomLogHandler::class,
                'via' => CustomLogger::class,
            ],
        ]);

        $router = $this->app['router'];
        $router->aliasMiddleware('timetoken_auth', HubApiTimeTokenAuth::class);
        $router->aliasMiddleware('dev_api_token', DevApiAuthToken::class);
        $router->aliasMiddleware('auth.local', LocalApiAuth::class);
    }

    public function boot()
    {
        $this->registerRoutes();
    }

    protected function registerRoutes(): void
    {
        Route::group([
            'prefix' => 'api',
        ], function () {
            $this->loadRoutesFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'routes' . DIRECTORY_SEPARATOR . 'api.php');
        });
    }
}
