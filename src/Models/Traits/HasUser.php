<?php

namespace Superius\OmniHub\Models\Traits;

use Superius\OmniHub\Helpers\HubApiHelper;

trait HasUser
{
    public function getUserDataAttribute()
    {
        $user = [];
        if ($this->user_id) {
            $user = HubApiHelper::getUser($this->user_id);
        }
        return $user;
    }
}
