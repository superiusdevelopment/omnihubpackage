<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Http\Client\Response;

abstract class HubApi
{
    private $host;
    private $uri;

    abstract static function getToken(): string;

    abstract static function getRequest(): \Illuminate\Http\Client\PendingRequest;

    public function __construct(string $host = null)
    {
        $this->host = $host;
    }

    public function getJson(string $uri, bool $returnError = false): array
    {
        $response = $this->get($uri);

        if(!$returnError && $response->clientError()){
            return [];
        }
        return JsonHelper::decode($response->body());
    }

    public function get(string $uri): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->get($this->getUrl());
    }

    private function getUrl(): string
    {
        $url = '';
        if ($this->host) {
            $url .= $this->host;
        } else {
            $url .= config('app.url');
        }

        if ($this->uri) {
            $url .= $this->uri;
        } else {
            throw new \RuntimeException('Trying to make request with empty uri');
        }
        return $url;
    }

    public function postJson(string $uri, array $data): array
    {
        $response = $this->post($uri, $data);
        return JsonHelper::decode($response->body());
    }

    public function post(string $uri, array $data): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->post($this->getUrl(), $data);
    }

    public function putJson(string $uri, array $data): array
    {
        $response = $this->put($uri, $data);
        return JsonHelper::decode($response->body());
    }

    public function put(string $uri, array $data): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->put($this->getUrl(), $data);
    }

    public function delete(string $uri, ?array $data): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->delete($this->getUrl(), $data);
    }
}
