<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum BillingStatusEnum: string
{
    use EnumAsSelectArray;

    case RESOLVED = 'Resolved';
    case SUSPENDED = 'Suspended';
    case SNOOZED = 'Snoozed';
}

