<?php

namespace Superius\OmniHub\Guards;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Superius\OmniHub\Helpers\MarketContext;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Class SessionTokenGuard
 * @package App\Guards
 *
 * This guard authenticate user based on session
 * that is opened in "regular" web page.
 * Must be used with redis sessions
 */
class SessionTokenGuard implements Guard
{
    use GuardHelpers;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function user(): ?Authenticatable
    {
        if ($this->user !== null) {
            return $this->user;
        }

        $token = $this->request->bearerToken();
        if ($token) {
            $userData = $this->getUserDataFromRedisSessionData($token);
            if ($userData) {
                $this->user = new User($userData);
            }
        }

        return $this->user;
    }

    private function getUserDataFromRedisSessionData(string $token): ?array
    {
        $sessionId = Crypt::decryptString($token);

        //read directly from redis session storage
        $sessionDataSerialized = Redis::connection(config('session.connection'))->get(config('cache.prefix') . $sessionId);

        if ($sessionDataSerialized) {   //double serialization
            $sessionData = unserialize($sessionDataSerialized, ['allowed_classes' => false]);
            $sessionData = unserialize($sessionData, ['allowed_classes' => false]);

            //not realy place for this...
            MarketContext::getInstance()->setMarket(data_get($sessionData, 'market'));
            \App::setlocale(data_get($sessionData, 'locale'));

            return data_get($sessionData, 'auth_user');
        }
        return null;
    }

    public function validate(array $credentials = []): bool
    {
        return false; //username and password "login" not allowed here
    }
}
