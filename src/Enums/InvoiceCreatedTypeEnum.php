<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum InvoiceCreatedTypeEnum: string
{
    use EnumAsSelectArray;

    case AUTO = 'auto';
    case CANCEL = 'cancel';
    case MANUAL = 'manual';
    case MONTHLY = 'monthly';
}
