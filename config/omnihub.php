<?php
/**
 * default omnihub configuration
 * this file is copied from package to omnihub config dir with:
 * php artisan vendor:publish --provider="Superius\OmniHub\Providers\HubServiceProvider"
 */
return [
    'a2a_api_key' => env('A2A_API_KEY'),
    'cache' => [
        'lifetime' => env('OMNIHUB_CACHE_LIFETIME', 3600)
    ],
    'debug' => [
        'remote_address' => env('DEBUG_REMOTE_ADDRESS', '127.0.0.1'),
    ],
];
