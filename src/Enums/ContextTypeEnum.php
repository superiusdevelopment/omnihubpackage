<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum ContextTypeEnum: string
{
    use EnumAsSelectArray;

    case CUSTOMER = 'customer';
    case PARTNER = 'partner';
}
