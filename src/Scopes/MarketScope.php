<?php

namespace Superius\OmniHub\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Superius\OmniHub\Helpers\MarketContext;

class MarketScope implements Scope
{

    public function apply(Builder $builder, Model $model): void
    {

        $mid = MarketContext::getInstance()->getMarketId();

        if (!$mid) {
            throw new \RuntimeException('Mid is missing in Market scope!');
        }

        $builder->where($model->getTable() . '.mid', '=', $mid);
    }
}
