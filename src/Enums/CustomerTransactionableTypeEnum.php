<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum CustomerTransactionableTypeEnum: string
{
    use EnumAsSelectArray;

    case STATEMENT_ITEM = 'statement_item';
}
