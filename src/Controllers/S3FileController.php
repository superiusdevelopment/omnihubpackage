<?php

namespace Superius\OmniHub\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use function Safe\base64_decode;
use function Safe\gmdate;
use function Safe\sprintf;
use function Safe\strtotime;

class S3FileController extends Controller
{
    /**
     * @throws \Safe\Exceptions\UrlException
     * @throws \Safe\Exceptions\DatetimeException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \Safe\Exceptions\StringsException
     */
    public function show(string $path): Response
    {
        $path = base64_decode($path);
        $file = Storage::cloud()->get($path);
        $mimeType = Storage::cloud()->mimeType($path);
        $lastModified = Storage::cloud()->lastModified($path);

        return response($file, Response::HTTP_OK, [
            'Content-Type' => $mimeType,
            'Expires' => $this->httpDate(strtotime('+1 year')),
            'Cache-Control' => 'public, max-age=31536000',
            'Last-Modified' => $this->httpDate($lastModified),
        ]);
    }

    /**
     * @throws \Safe\Exceptions\DatetimeException
     * @throws \Safe\Exceptions\StringsException
     */
    protected function httpDate(int $timestamp): string
    {
        return sprintf('%s GMT', gmdate('D, d M Y H:i:s', $timestamp));
    }
}
