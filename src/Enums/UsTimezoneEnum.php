<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum UsTimezoneEnum: string
{
    use EnumAsSelectArray;

    case ADAK = 'America/Adak';
    case ANCHORAGE = 'America/Anchorage';
    case BOISE = 'America/Boise';
    case CHICAGO = 'America/Chicago';
    case DENVER = 'America/Denver';
    case DETROIT = 'America/Detroit';
    case INDIANA_INDIANAPOLIS = 'America/Indiana/Indianapolis';
    case INDIANA_KNOX = 'America/Indiana/Knox';
    case INDIANA_MARENGO = 'America/Indiana/Marengo';
    case INDIANA_PETERSBURG = 'America/Indiana/Petersburg';
    case INDIANA_TELL_CITY = 'America/Indiana/Tell_City';
    case INDIANA_VEVAY = 'America/Indiana/Vevay';
    case INDIANA_VINCENNES = 'America/Indiana/Vincennes';
    case INDIANA_WINAMAC = 'America/Indiana/Winamac';
    case JUNEAU = 'America/Juneau';
    case KENTUCKY_LOUISVILLE = 'America/Kentucky/Louisville';
    case KENTUCKY_MONTICELLO = 'America/Kentucky/Monticello';
    case LOS_ANGELES = 'America/Los_Angeles';
    case MENOMINEE = 'America/Menominee';
    case METLAKATLA = 'America/Metlakatla';
    case NEW_YORK = 'America/New_York';
    case NOME = 'America/Nome';
    case NORTH_DAKOTA_BEULAH = 'America/North_Dakota/Beulah';
    case NORTH_DAKOTA_CENTER = 'America/North_Dakota/Center';
    case NORTH_DAKOTA_NEW_SALEM = 'America/North_Dakota/New_Salem';
    case PHOENIX = 'America/Phoenix';
    case SITKA = 'America/Sitka';
    case YAKUTAT = 'America/Yakutat';
    case HONOLULU = 'Pacific/Honolulu';

    public function getLabel(): string
    {
        return match ($this) {
            self::ADAK => 'Adak Time',
            self::ANCHORAGE => 'Anchorage Time',
            self::BOISE => 'Boise Time',
            self::CHICAGO => 'Central Time (CT)',
            self::DENVER => 'Mountain Time (MT)',
            self::DETROIT => 'Eastern Time (ET)',
            self::INDIANA_INDIANAPOLIS => 'Indiana (Indianapolis) Time',
            self::INDIANA_KNOX => 'Indiana (Knox) Time',
            self::INDIANA_MARENGO => 'Indiana (Marengo) Time',
            self::INDIANA_PETERSBURG => 'Indiana (Petersburg) Time',
            self::INDIANA_TELL_CITY => 'Indiana (Tell City) Time',
            self::INDIANA_VEVAY => 'Indiana (Vevay) Time',
            self::INDIANA_VINCENNES => 'Indiana (Vincennes) Time',
            self::INDIANA_WINAMAC => 'Indiana (Winamac) Time',
            self::JUNEAU => 'Juneau Time',
            self::KENTUCKY_LOUISVILLE => 'Kentucky (Louisville) Time',
            self::KENTUCKY_MONTICELLO => 'Kentucky (Monticello) Time',
            self::LOS_ANGELES => 'Pacific Time (PT)',
            self::MENOMINEE => 'Menominee Time',
            self::METLAKATLA => 'Metlakatla Time',
            self::NEW_YORK => 'Eastern Time (ET)',
            self::NOME => 'Nome Time',
            self::NORTH_DAKOTA_BEULAH => 'North Dakota (Beulah) Time',
            self::NORTH_DAKOTA_CENTER => 'North Dakota (Center) Time',
            self::NORTH_DAKOTA_NEW_SALEM => 'North Dakota (New Salem) Time',
            self::PHOENIX => 'Phoenix Time (No DST)',
            self::SITKA => 'Sitka Time',
            self::YAKUTAT => 'Yakutat Time',
            self::HONOLULU => 'Hawaii-Aleutian Time (No DST)',
        };
    }
}
