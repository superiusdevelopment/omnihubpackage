<?php

namespace Superius\OmniHub\Models\Traits;

use Superius\OmniHub\Helpers\HubApiHelper;
use Superius\OmniHub\Enums\ContextTypeEnum;

trait HasCustomerable
{

    /** @var Customerable $customerable */

    public Customerable $customerable_cache;

    public function getCustomerableAttribute():Customerable
    {
        if(isset($this->customerable_cache)){
            return $this->customerable_cache;
        }

        $customerableData = [];

        if ($this->customerable_id){
            if($this->customerable_type == ContextTypeEnum::CUSTOMER->value)  {
                $customerableData = HubApiHelper::getCustomer($this->customerable_id);
            }
            elseif($this->customerable_type == ContextTypeEnum::PARTNER->value){
                $customerableData = HubApiHelper::getPartner($this->customerable_id);
            }
        }

        $this->customerable_cache = Customerable::make($customerableData, $this->customerable_type);

        return $this->customerable_cache;
    }
}
