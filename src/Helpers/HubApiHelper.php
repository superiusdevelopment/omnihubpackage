<?php

namespace Superius\OmniHub\Helpers;

use Cache;

class HubApiHelper
{
    public static function getUsers(): array
    {
        $cacheKey = MarketContext::isNotSet() ? 'users' : 'users_' . MarketContext::getMarketSlug();

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }
        $response = HubApiUserRequestBuilder::hub()->get('/api/users');
        $users = $response->json();

        if (!$response->ok() || empty($users)) {
            return [];
        }

        Cache::put($cacheKey, $users, config('omnihub.cache.lifetime'));

        return $users;
    }

    public static function getUser(string $userId): array
    {
        return Cache::remember('user-' . $userId, config('omnihub.cache.lifetime'), function () use ($userId) {
            return HubApiUserRequestBuilder::hub()->getJson('/api/users/' . $userId);
        });
    }

    public static function getCustomer(string $customerId): array
    {
        return Cache::remember(
            'customer-' . $customerId,
            config('omnihub.cache.lifetime'),
            function () use ($customerId) {
                return HubLocalApiRequestBuilder::customersHub()
                    ->getJson('/api/a2a/customers/get-by-id/' . $customerId);
            }
        );
    }

    public static function searchCustomers(array $searchBy): array
    {
        return HubApiUserRequestBuilder::customersHub()->postJson('/api/customers/search', $searchBy);
    }

    public static function getPartner(mixed $partner_id)
    {
        return Cache::remember(
            'partner-' . $partner_id,
            config('omnihub.cache.lifetime'),
            function () use ($partner_id) {
                return HubLocalApiRequestBuilder::partnersHub()
                    ->getJson('/api/a2a/partners/get-by-id/' . $partner_id);
            }
        );
    }
}
