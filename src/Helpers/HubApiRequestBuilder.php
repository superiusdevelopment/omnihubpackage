<?php

namespace Superius\OmniHub\Helpers;

/**
 * @method static HubApiRequest billingHub()
 * @method static HubApiRequest billingRsHub()
 * @method static HubApiRequest cclHub()
 * @method static HubApiRequest commissionsHub()
 * @method static HubApiRequest customersHub()
 * @method static HubApiRequest dunningHub()
 * @method static HubApiRequest faqHub()
 * @method static HubApiRequest fm()
 * @method static HubApiRequest invoicesHub()
 * @method static HubApiRequest minimaxHub()
 * @method static HubApiRequest hub()
 * @method static HubApiRequest neosalonHub()
 * @method static HubApiRequest nextstepHub()
 * @method static HubApiRequest pantheonHub()
 * @method static HubApiRequest partnersHub()
 * @method static HubApiRequest phoneHub()
 * @method static HubApiRequest quotes2Hub()
 * @method static HubApiRequest statementsHub()
 * @method static HubApiRequest subscriptionsHub()
 * @method static HubApiRequest ticketsHub()
 * @method static HubApiRequest utilitiesHub()
 * @method static HubApiRequest warehousesHub()
 * @method static HubApiRequest setupHub()
 * @method static HubApiRequest reportingHub()
 */
class HubApiRequestBuilder extends HubUrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return \Superius\OmniHub\Helpers\HubApiRequest
     */
    public static function __callStatic(string $name, array $arguments): HubApiRequest
    {
        return self::apiRequest(self::formatUrlKey($name));
    }

    private static function apiRequest(string $hubUrlKey): HubApiRequest
    {
        $url = self::getUrlByKey($hubUrlKey);

        return new HubApiRequest($url);
    }
}
