<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Support\Facades\Log;
use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class HubExceptionHandler extends ExceptionHandler
{
    public function report(Throwable $exception)
    {
        if ($this->shouldReport($exception)) {
            if (app()->bound('sentry')) {
                app('sentry')->captureException($exception);
            }

            Log::error($exception->getMessage() . '. Status: ' . $exception->getCode(), [
                'method' => __METHOD__ . ':' . __LINE__,
            ]);
        }

        parent::report($exception);
    }
}
