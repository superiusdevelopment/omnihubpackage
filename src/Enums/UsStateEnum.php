<?php

namespace Superius\OmniHub\Enums;

use Illuminate\Support\Str;
use Superius\OmniHub\Traits\EnumAsSelectArray;

enum UsStateEnum: string
{
    use EnumAsSelectArray;

    case ALABAMA = 'ALABAMA'; // AL
    case ALASKA = 'ALASKA'; // AK
    case ARIZONA = 'ARIZONA'; // AZ
    case ARKANSAS = 'ARKANSAS'; // AR
    case CALIFORNIA = 'CALIFORNIA'; // CA
    case COLORADO = 'COLORADO'; // CO
    case CONNECTICUT = 'CONNECTICUT'; // CT
    case DELAWARE = 'DELAWARE'; // DE
    case FLORIDA = 'FLORIDA'; // FL
    case GEORGIA = 'GEORGIA'; // GA
    case HAWAII = 'HAWAII'; // HI
    case IDAHO = 'IDAHO'; // ID
    case ILLINOIS = 'ILLINOIS'; // IL
    case INDIANA = 'INDIANA'; // IN
    case IOWA = 'IOWA'; // IA
    case KANSAS = 'KANSAS'; // KS
    case KENTUCKY = 'KENTUCKY'; // KY
    case LOUISIANA = 'LOUISIANA'; // LA
    case MAINE = 'MAINE'; // ME
    case MARYLAND = 'MARYLAND'; // MD
    case MASSACHUSETTS = 'MASSACHUSETTS'; // MA
    case MICHIGAN = 'MICHIGAN'; // MI
    case MINNESOTA = 'MINNESOTA'; // MN
    case MISSISSIPPI = 'MISSISSIPPI'; // MS
    case MISSOURI = 'MISSOURI'; // MO
    case MONTANA = 'MONTANA'; // MT
    case NEBRASKA = 'NEBRASKA'; // NE
    case NEVADA = 'NEVADA'; // NV
    case NEW_HAMPSHIRE = 'NEW_HAMPSHIRE'; // NH
    case NEW_JERSEY = 'NEW_JERSEY'; // NJ
    case NEW_MEXICO = 'NEW_MEXICO'; // NM
    case NEW_YORK = 'NEW_YORK'; // NY
    case NORTH_CAROLINA = 'NORTH_CAROLINA'; // NC
    case NORTH_DAKOTA = 'NORTH_DAKOTA'; // ND
    case OHIO = 'OHIO'; // OH
    case OKLAHOMA = 'OKLAHOMA'; // OK
    case OREGON = 'OREGON'; // OR
    case PENNSYLVANIA = 'PENNSYLVANIA'; // PA
    case RHODE_ISLAND = 'RHODE_ISLAND'; // RI
    case SOUTH_CAROLINA = 'SOUTH_CAROLINA'; // SC
    case SOUTH_DAKOTA = 'SOUTH_DAKOTA'; // SD
    case TENNESSEE = 'TENNESSEE'; // TN
    case TEXAS = 'TEXAS'; // TX
    case UTAH = 'UTAH'; // UT
    case VERMONT = 'VERMONT'; // VT
    case VIRGINIA = 'VIRGINIA'; // VA
    case WASHINGTON = 'WASHINGTON'; // WA
    case WEST_VIRGINIA = 'WEST_VIRGINIA'; // WV
    case WISCONSIN = 'WISCONSIN'; // WI
    case WYOMING = 'WYOMING'; // WY

    public function getLabel(): string
    {
        return Str::title(str_replace('_', ' ', strtolower($this->value)));
    }
}
