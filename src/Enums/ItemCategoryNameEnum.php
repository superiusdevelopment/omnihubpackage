<?php

namespace Superius\OmniHub\Enums;

enum ItemCategoryNameEnum: string
{
    case ALL_IN_ONE = 'all_in_one';
    case SIM = 'sim';
}
