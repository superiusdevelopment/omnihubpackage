<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Support\Facades\Cache;

class HubUsersCacheHelper
{
    /**
     * @return array<int,array<string,mixed>>|null
     */
    public static function getUsers(): ?array
    {
        return Cache::remember(
            'users-' . MarketContext::getMarketSlug(),
            config('omnihub.cache.lifetime'),
            function () {
                $users = HubApiUserRequestBuilder::hub()->getJson('/api/users');

                if (!empty($users)) {
                    return $users;
                }

                return null;
            }
        );
    }
}
