<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum PaymentTypeEnum: string
{
    use EnumAsSelectArray;

    case QUOTE = 'quote';
    case INVOICE = 'invoice';
}
