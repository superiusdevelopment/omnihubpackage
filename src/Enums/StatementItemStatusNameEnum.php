<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum StatementItemStatusNameEnum: string
{
    use EnumAsSelectArray;

    case IN_PROCESS = 'in_process';
    case UNRESOLVED = 'unresolved';
    case RESOLVED = 'resolved';
    case INCOMPLETE = 'incomplete';
    case SKIPPED = 'skipped';
    case UNPROCESSED = 'unprocessed';
}
