<?php

namespace Superius\OmniHub\Services\MailchimpService;

use Superius\OmniHub\Enums\MarketEnum;
use Superius\OmniHub\Helpers\MarketContext;
use Illuminate\Support\Str;

class GetListName
{
    public function getListName(?string $partnerType = null): string
    {
        $marketId = (string)MarketContext::getInstance()->getMarketId();

        if ($marketId === (string)MarketEnum::HR->value) {
            return 'mailchimp_hr';
        }

        if ($marketId === (string)MarketEnum::RS->value) {
            return 'mailchimp_rs';
        }

        if ($marketId === (string)MarketEnum::IT->value) {
            return 'mailchimp_it';
        }

        if ($marketId === (string)MarketEnum::SI->value) {
            return 'mailchimp_slo';
        }

        if ($marketId === (string)MarketEnum::US->value) {
            if ($partnerType) {
                return 'mailchimp_us_partner_' . Str::replace(' ', '_', $partnerType);;
            }

            return 'mailchimp_us';
        }

        abort(404, 'List not found.');
    }
}
