<?php


namespace Superius\OmniHub\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

/**
 * Class SetDebugModeCommand
 * @package Superius\OmniHub\Commands
 * If environment isn't in local or dev mode, this command change status of debug mode.
 */
class SetDebugModeCommand extends Command
{
    protected $signature = 'omnihub:setdebug {flag}';
    protected $description = 'Turn debug mode on/off';

    final public function handle(): void
    {
        if (App::environment(['local', 'dev'])) {
            $this->info("Current environment is " . App::environment() . ". This command cannot be executed.");
            return;
        }

        $flag = $this->argument('flag');

        if ($flag != "on" && $flag != "off") {
            $this->info("This command cannot be executed. Invalid flag! Flag must be 'on' or 'off', e.g. 'omnihub:setdebug on'.");
            return;
        }

        $envFile = app()->environmentFilePath();
        $is_debug_true = config('app.debug');
        $str_env = file_get_contents($envFile);

        if ($flag === 'on' && !$is_debug_true) {
            $str_env = str_replace("APP_DEBUG=false", "APP_DEBUG=true", $str_env);
        } elseif ($flag === 'off' && $is_debug_true) {
            $str_env = str_replace("APP_DEBUG=true", "APP_DEBUG=false", $str_env);
        } else {
            $this->info("This command cannot be executed because APP_DEBUG is " . ($is_debug_true ? "true." : "false."));
            return;
        }
        file_put_contents($envFile, $str_env);

        Artisan::call('config:cache');
        $this->info("Artisan command is successfully executed. Current debug mode is: " . (config('app.debug') ? 'true' : 'false'));
    }
}
