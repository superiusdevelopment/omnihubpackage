<?php

namespace Superius\OmniHub\Enums;

enum WarehouseItemTypeNameEnum: string
{
    case S30 = 's30';
    case SUNMI_V1 = 'sunmi_v1';
    case SUNMI_V2 = 'sunmi_v2';
    case SUNMI_V2_PRO = 'sunmi_v2_pro';
    case SUNMI_V2_S = 'sunmi_v2_s';
    case TCOM_SIM = 't_com_sim';
    case YETTEL_SIM = 'yettel_sim';
}
