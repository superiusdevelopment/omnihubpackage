<?php

namespace Superius\OmniHub\A2ARequest;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class A2AApiToken
{
    public ?int $mid;
    public ?string $tid;
    private int $ts;
    private string $salt;
    private string $signature;
    private bool $isDemo;

    public function __construct(int $mid = null, string $tid = null, bool $isDemo = false)
    {
        $this->ts = Carbon::now('UTC')->timestamp;
        $this->salt = Config::get('omnihub.a2a_api_key');
        $this->mid = $mid;
        $this->tid = $tid;
        $this->isDemo = $isDemo;
    }

    /**
     * @throws \JsonException
     */
    public function toString(): string
    {
        $this->makeSignature();
        return base64_encode(json_encode($this->getPayload(true), JSON_THROW_ON_ERROR));
    }

    public function makeSignature(): void
    {
        $this->signature = Hash::make($this->getSaltedPayloadString());
    }

    private function getSaltedPayloadString(): string
    {
        return json_encode($this->ts) . $this->salt;
    }

    public function getPayload(bool $withSignature = false): array
    {
        $payload = [
            'ts' => $this->ts,
        ];

        if ($this->mid && $this->tid) {
            $payload['context'] = ['mid' => $this->mid, 'tid' => $this->tid];
        }

        if ($withSignature && isset($this->signature)) {
            $payload['signature'] = $this->signature;
        }

        if ($this->isDemo) {
            $payload['is_demo'] = true;
        }

        return $payload;
    }

    public function fromToken(string $tokenStr): self
    {
        $tokenPayload = json_decode(\Safe\base64_decode($tokenStr), true);
        $this->ts = data_get($tokenPayload, 'ts', 0);
        $this->signature = data_get($tokenPayload, 'signature', '');
        $this->mid = data_get($tokenPayload, 'context.mid');
        $this->tid = data_get($tokenPayload, 'context.tid');
        $this->isDemo = data_get($tokenPayload, 'is_demo', false);

        return $this;
    }

    public function isValid(): bool
    {
        // Provjera da li je u requestu poslan ispravan format datuma te da li je razlika od slanja requesta pa
        // do zaprimanja manja od 5 sekundi.
        $date = Carbon::createFromTimestampUTC($this->ts);
        if (!$date->isValid() || $date->diffInSeconds() > 5) {
            return false;
        }

        return Hash::check($this->getSaltedPayloadString(), $this->signature);
    }

    public function hasContextPayload(): bool
    {
        return $this->mid && $this->tid;
    }
}
