<?php

namespace Superius\OmniHub\Enums;

use Illuminate\Support\Str;

enum MarketEnum: int
{
    case HR = 1;

    case IT = 2;

    case SI = 3;

    case RS = 4;

    case US = 5;

    public static function getMarketEnumPattern(): string
    {
        $marketEnumPattern = '';
        foreach (self::cases() as $marketEnum) {
            $marketEnumPattern .= Str::lower($marketEnum->name);
            $marketEnumPattern .= '|';
        }
        return Str::substr($marketEnumPattern, 0, -1);
    }

    public static function getEnumCaseValueByName(string $name): int
    {
        foreach (self::cases() as $marketEnum) {
            if (($marketEnum->name) === Str::upper($name)) {
                return $marketEnum->value;
            }
        }

        abort(404, 'Market enum not found.');
    }
}
