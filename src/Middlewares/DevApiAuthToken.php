<?php

namespace Superius\OmniHub\Middlewares;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class DevApiAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->bearerToken() === config('omnihub.dev_api_key')) {
            return $next($request);
        }

        throw new AuthenticationException('Invalid token.');
    }
}
