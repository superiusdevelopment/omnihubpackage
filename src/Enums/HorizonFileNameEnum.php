<?php

namespace Superius\OmniHub\Enums;

enum HorizonFileNameEnum: string
{
    case WHITE_LIST_FILE = 'horizon_ip_whitelist';
}
