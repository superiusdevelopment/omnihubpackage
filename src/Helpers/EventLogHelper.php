<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Http\Client\Response;

class EventLogHelper
{
    public static function createEvent(
        string $name,
        string $sourceableId,
        string $sourceableType,
        string $resultableId,
        string $resultableType,
        string $customerableId,
        string $customerableType,
        ?string $description = null
    ): Response {
        $data = [
            'name' => $name,
            'sourceable_id' => $sourceableId,
            'sourceable_type' => $sourceableType,
            'resultable_id' => $resultableId,
            'resultable_type' => $resultableType,
            'customerable_id' => $customerableId,
            'customerable_type' => $customerableType,
        ];

        if ($description) {
            data_set($data, 'description', $description);
        }

        return HubApiRequestBuilder::billingHub()->post('/api/events', $data);
    }

    public static function createEventLog(string $eventId, string $description): Response
    {
        return HubApiRequestBuilder::billingHub()
            ->post('/api/event-logs', [
                'event_id' => $eventId,
                'description' => $description,
            ]);
    }
}
