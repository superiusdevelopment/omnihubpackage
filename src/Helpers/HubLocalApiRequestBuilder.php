<?php

namespace Superius\OmniHub\Helpers;

/**
 * @method static HubApiLocalRequest billingHub()
 * @method static HubApiLocalRequest billingRsHub()
 * @method static HubApiLocalRequest cclHub()
 * @method static HubApiLocalRequest commissionsHub()
 * @method static HubApiLocalRequest customersHub()
 * @method static HubApiLocalRequest dunningHub()
 * @method static HubApiLocalRequest faqHub()
 * @method static HubApiLocalRequest fmHub()
 * @method static HubApiLocalRequest invoicesHub()
 * @method static HubApiLocalRequest minimaxHub()
 * @method static HubApiLocalRequest hub()
 * @method static HubApiLocalRequest neosalonHub()
 * @method static HubApiLocalRequest nextstepHub()
 * @method static HubApiLocalRequest pantheonHub()
 * @method static HubApiLocalRequest partnersHub()
 * @method static HubApiLocalRequest phoneHub()
 * @method static HubApiLocalRequest quotes2Hub()
 * @method static HubApiLocalRequest statementsHub()
 * @method static HubApiLocalRequest subscriptionsHub()
 * @method static HubApiLocalRequest ticketsHub()
 * @method static HubApiLocalRequest utilitiesHub()
 * @method static HubApiLocalRequest warehousesHub()
 * @method static HubApiRequest setupHub()
 * @method static HubApiRequest reportingHub()
 */
class HubLocalApiRequestBuilder extends HubUrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return \Superius\OmniHub\Helpers\HubApiLocalRequest
     */
    public static function __callStatic(string $name, array $arguments): HubApiLocalRequest
    {
        return self::apiRequest(self::formatUrlKey($name));
    }

    private static function apiRequest(string $hubUrlKey): HubApiLocalRequest
    {
        $url = self::getUrlByKey($hubUrlKey, true);

        return new HubApiLocalRequest($url);
    }
}
