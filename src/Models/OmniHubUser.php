<?php

namespace Superius\OmniHub\Models;

use Superius\OmniHub\Models\Traits\HasUserGroup;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @description User model on "sub apps" should be just:
 * class User extends OmniHubUser {}
 */
class OmniHubUser extends Authenticatable
{
    use HasUserGroup;

    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'name',
        'email',
        'group'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

}
