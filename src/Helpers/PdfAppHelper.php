<?php

namespace Superius\OmniHub\Helpers;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class PdfAppHelper
{
    /**
     * @param string $url
     * @param array<string,mixed> $data
     * @return \Illuminate\Http\Client\Response
     */
    public static function post(string $url, array $data): Response
    {
        $response = Http::withToken(config('omnihub.pdfapp_api_token'))
            ->post(config('omnihub.url.pdfapp') . $url, $data);

        if ($response->failed()) {
            abort($response->status(), 'PdfApp response with error code: ' . $response->status() .
                '. Message: ' . $response->body());
        }

        return $response;
    }
}
