<?php

namespace Superius\OmniHub\Enums;

use Superius\OmniHub\Traits\EnumAsSelectArray;

enum NextStepTypeEnum: string
{
    use EnumAsSelectArray;

    case BAN = 'BAN';
    case BILLING = 'BILLING';
    case BOARDING = 'BOARDING';
    case CARDS = 'CARDS';
    case CHARGEBACK = 'CHARGEBACK';
    case CUSTOMER_TRANSACTION = 'CUSTOMER TRANSACTION';
    case DEBT_COLLECTION = 'DEBT COLLECTION';
    case DISPUTE = 'DISPUTE';
    case NEW = 'NEW';
    case PARTNERPORTAL_QUOTE = 'PARTNERPORTAL QUOTE';
    case PAYMENT = 'PAYMENT';
    case QUOTE = 'QUOTE';
    case RISK_HOLD = 'RISK HOLD';
    case STATEMENT_ITEM = 'STATEMENT ITEM';
    case TODO = 'TODO';
    case TRADE_IN = 'TRADE-IN';
    case WEB_QUOTE = 'WEB QUOTE';
    case MANUALLY_DECLINED = 'MANUALLY DECLINED';
    case NEED_INFORMATION = 'NEED INFORMATION';
    case RISK_VALIDATION_REQUESTED = 'RISK VALIDATION REQUESTED';
    case ACCEPTED = 'ACCEPTED';
    case CONTESTED = 'CONTESTED';
    case CREATED = 'CREATED';
    case UPDATED = 'UPDATED';

    public function priority(): int
    {
        return match ($this) {
            self::BAN => 24,
            self::BILLING => 2,
            self::BOARDING => 5,
            self::CARDS => 4,
            self::CHARGEBACK => 14,
            self::CUSTOMER_TRANSACTION => 1,
            self::DEBT_COLLECTION => 7,
            self::DISPUTE => 15,
            self::NEW => 13,
            self::PARTNERPORTAL_QUOTE => 10,
            self::PAYMENT => 6,
            self::QUOTE => 9,
            self::RISK_HOLD => 16,
            self::STATEMENT_ITEM => 11,
            self::TODO => 3,
            self::TRADE_IN => 12,
            self::WEB_QUOTE => 8,
            self::MANUALLY_DECLINED => 17,
            self::NEED_INFORMATION => 18,
            self::RISK_VALIDATION_REQUESTED => 19,
            self::ACCEPTED => 20,
            self::CONTESTED => 21,
            self::CREATED => 22,
            self::UPDATED => 23,
        };
    }
}
