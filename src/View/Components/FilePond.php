<?php

namespace Superius\OmniHub\View\Components;

use Illuminate\View\Component;

class FilePond extends Component
{
    public $name;

    public function __construct(string $name = null)
    {
        $this->name = $name;
    }

    public function render()
    {
        return view('omnihub::components.filepond');
    }
}
