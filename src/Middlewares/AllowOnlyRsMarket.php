<?php

namespace Superius\OmniHub\Middlewares;

use Brian2694\Toastr\Facades\Toastr;
use Closure;
use Illuminate\Http\Request;
use Superius\OmniHub\Enums\MarketEnum;
use Superius\OmniHub\Helpers\HubUrlBuilder;
use Superius\OmniHub\Helpers\MarketContext;
use Symfony\Component\HttpFoundation\Response;

class AllowOnlyRsMarket
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (MarketContext::getInstance()->getMarketId() !== MarketEnum::RS->value) {
            Toastr::error("Only RS market allowed");
            return redirect(HubUrlBuilder::hub());
        }

        return $next($request);
    }
}
