<?php

namespace Superius\OmniHub\Enums;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Služi da bi se šerali id-jevi subscriptiona
 * Ovaj isti enum se nalazi na invoices, customers i subscriptions hubu
 */
enum SubscriptionSeederEnum: string
{
    case CARD_PAYMENT_SERVICE = 'Subscription:Usluga kartičnog plaćanja';
    case DEVICE_RENTAL = 'Subscription:Najam uređaja';
    case INTERNET = 'Subscription:Mobilni internet';
    case NEOSALON = 'Subscription:Neosalon';
    case PLUS_PACKAGE = 'Subscription:Plus paket';
    case MARKETINO_SI = 'Subscription:Si:Marketino';

    public function uuid(): UuidInterface
    {
        return Uuid::uuid5(Uuid::NAMESPACE_OID, $this->value);
    }
}
