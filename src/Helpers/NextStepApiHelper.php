<?php

namespace Superius\OmniHub\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Client\Response;
use Superius\OmniHub\Enums\NextStepTypeEnum;

class NextStepApiHelper
{
    public static function setNextStep(
        string $subject,
        string $comment,
        string $customerableId = null,
        string $customerableType = null,
        string $type = null,
        int|string|null $userId = null,
        string $date = null,
        string $resourceId = null,
    ): Response {
        return HubApiRequestBuilder::nextstepHub()->post('/api/add-nextstep', [
            'customerable_id' => $customerableId,
            'customerable_type' => $customerableType,
            'type' => $type ?: NextStepTypeEnum::TODO->value,
            'subject' => $subject,
            'user_id' => $userId,
            'comment' => $comment,
            'date' => $date ?: Carbon::today()->toDateString(),
            'resource_id' => $resourceId,
        ]);
    }
}
