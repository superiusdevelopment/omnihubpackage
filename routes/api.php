<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Superius\OmniHub\Controllers\HorizonController;

Route::prefix('dev')->group(function () {
    Route::group(['middleware' => 'dev_api_token', 'prefix' => 'horizon'], function () {
        Route::post('/whitelist/add-ip-address', [HorizonController::class, 'addIpAddressOnWhitelist']);
        Route::post('/whitelist/delete', [HorizonController::class, 'removeWhitelist']);
    });

    Route::get('/ping', function () {
        try {
            DB::connection()->getPdo();
            return response()->json(['message' => "Connected successfully to: " . DB::connection()->getDatabaseName()]);
        } catch (\Exception $e) {
            return response()->json(['message' => "Could not connect to the database."], 500);
        }
    });
});
