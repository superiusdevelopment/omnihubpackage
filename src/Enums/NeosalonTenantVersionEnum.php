<?php

namespace Superius\OmniHub\Enums;

enum NeosalonTenantVersionEnum: string
{
    case SALON_PLUS = 'SALON_PLUS';
    case SALON_MINI = 'SALON_MINI';
}
